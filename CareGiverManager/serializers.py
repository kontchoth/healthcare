from django.contrib.auth.models import User
import logging
from rest_framework import serializers
from healthcare.serializers import AddressSerializer
from .constants import AVATAR_DIR

from .models import *

class ClassificationSerializer(serializers.ModelSerializer):
	label = serializers.SerializerMethodField()
	class Meta:
		model = Classification
		fields = '__all__'

	def get_label(self, classification):
		return classification.name

class PersonalDataSerializer(serializers.ModelSerializer):
	avatar = serializers.SerializerMethodField()
	primary_address = AddressSerializer()
	classification = ClassificationSerializer()
	hired_date = serializers.SerializerMethodField()
	
	class Meta:
		model = PersonalData
		fields = '__all__'

	def get_avatar(self, care_giver):
		avatar = care_giver.avatar
		if avatar:
			return '/media/avatar' + avatar.url.split('avatarts')[-1]
		return ''

	def get_hired_date(self, obj):
		if obj.hired_date:
			return obj.hired_date.strftime('%B %d, %Y')
		return None

class CareGiverSerializer(serializers.ModelSerializer):
	personal_data = PersonalDataSerializer()

	class Meta:
		model = CareGiver
		fields = '__all__'

class NoteSerializer(serializers.ModelSerializer):
	class Meta:
		model = Note
		fields = '__all__'

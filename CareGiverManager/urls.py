from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from CareGiverManager import views

urlpatterns = [
	url(r'^care-givers/all$', views.get_all_caregivers),
	url(r'^care-givers/personal-data$', views.personal_data_handler),
	url(r'^care-givers/personal-data/(?P<pk>[0-9a-zA-z]+)$', views.personal_data_handler),
	url(r'^care-givers/note/(?P<pk>[0-9a-zA-z]+)$', views.note_handler),
	url(r'^care-givers/(?P<pk>[0-9a-zA-z]+)/$', views.CareGiverDetail.as_view()),
	url(r'^care-givers/classifications/all$', views.ClassificationList.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
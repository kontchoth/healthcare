from __future__ import unicode_literals

import datetime
from django.db import models
from django.contrib.auth.models import User
from .constants import *
from django.db import IntegrityError
from .constants import *
from uuid import uuid4
from localflavor.us.models import *
from django.utils.translation import ugettext_lazy as _
from healthcare.models import Address


class Classification(models.Model):
	"""Care Giver Classification"""
	name = models.CharField(max_length=100, null=False, unique=True, blank=False)
	definition = models.CharField(max_length=200, null=True, blank=True)

	def __str__(self):
		return self.name

	def get_name(self):
		return self.name

	def get_definition(self):
		if self.definition:
			return self.definition
		return ''

class PersonalData(models.Model):
	avatar = models.ImageField(upload_to=AVATAR_DIR, null=True, blank=True) 
	firstname = models.CharField(_('First Name'), max_length=100, null=False, blank=False)
	middlename = models.CharField(_('Middle Name'), max_length=100, null=True, blank=True)
	lastname = models.CharField(_('Last Name'), max_length=100, null=False, blank=False)
	ssn = models.CharField(_('SSN'), blank=True, null=True, max_length=10)
	gender = models.CharField(_('Gender'), blank=False, null=False, max_length=10)
	birth_date = models.DateField(blank=False, null=False)
	email = models.EmailField(_('Email'), null=False, blank=False)
	primary_phone = models.CharField(_('Primary Phone Number'), max_length=10, null=False, blank=False)
	secondary_phone = models.CharField(_('Secondary Phone Number'), max_length=10, null=True, blank=True)
	classification = models.ForeignKey(Classification, null=False, blank=False)
	hired_date = models.DateField(blank=True, null=True, default=None)
	term_date = models.DateField(blank=True, null=True)
	state_licence_number = models.CharField(max_length=200, null=True, blank=True)
	primary_address = models.ForeignKey(Address, null=False, blank=False)
	secondary_address = models.ForeignKey(Address, related_name='+', null=True, blank=True)

class CareGiver(models.Model):
	STATUS_CHOICES = (
		('ACTIVE', _('Active')),
		('SUSPENDED', _('Suspended')),
		('INACTIVE', _('Inactive'))
	)
	id = models.CharField(primary_key=True, max_length=32, editable=False)
	status = models.CharField(_('Status'), max_length=20, choices=STATUS_CHOICES, default='INACTIVE')
	personal_data = models.ForeignKey(PersonalData, blank=True, null=True)
	created = models.DateTimeField(auto_now_add=True)
	last_modify = models.DateTimeField(auto_now_add=True)

	class Meta:
		ordering = ('personal_data__firstname',)

	

	def save(self, *args, **kwargs):
		'''
		Create a UUID for the profile and make sure it is unique
		'''
		if self.id:
			self.last_modify = datetime.datetime.now()
			super(CareGiver, self).save(*args, **kwargs)
			return
		unique = False
		while not unique:
			try:
				self.id = uuid4().hex
				super(CareGiver, self).save(*args, **kwargs)
				unique = True
			except IntegrityError:
				self.id = uuid4().hex

class Note(models.Model):
	caregiver = models.ForeignKey(CareGiver, blank=False, null=False, editable=False)
	author = models.CharField(_('Author'), blank=False, null=False, editable=False)
	date = models.DateTimeField(auto_now_add=True, editable=False)
	comment = models.TextField(null=False, blank=False, editable=False)

	def __str__(self):
		return self.comment




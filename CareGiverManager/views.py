from django.http import Http404
import json
from rest_framework.parsers import JSONParser
from rest_framework.decorators import parser_classes
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from CareGiverManager.models import *
from healthcare.models import *
from healthcare.serializers import *
from CareGiverManager.serializers import CareGiverSerializer
from CareGiverManager.serializers import ClassificationSerializer
from CareGiverManager.serializers import PersonalDataSerializer


def get_list(request, obj_type, obj_serializer):
	obj_list = obj_type.objects.all()
	serialiser = obj_serializer(obj_list, many=True)
	return serialiser.data

def get_obj(request, pk, obj_type, obj_serializer, serialize=False):
	try:
		obj = obj_type.objects.get(id=pk)
		if serialize:
			serialiser = obj_serializer(obj_type)
			return serialiser
		else:
			return obj
	except:
		return None

def find_match_or_create(kw_find, kw_create, obj_type):
	obj = obj_type.objects.get(**kw_find)	
	if not obj:
		print('Creating ')
		obj = obj_type(**kw_create)
		obj.save()
	return obj

def construct_find_options(option):
	options = {}
	for key in option.keys():
		options[key + '__iexact'] = option[key]
	return options

@api_view(['GET'])
def get_all_caregivers(request):
	caregivers = get_list(request, CareGiver, CareGiverSerializer)
	return Response({'results':caregivers})

@api_view(['POST', 'GET'])
def personal_data_handler(request, pk=None):
	if request.method == 'POST':
		data = request.data
		data['personal_data']['classification'] = get_obj(**{
			'request': request,
			'pk': data['personal_data']['classification']['id'],
			'obj_type': Classification,
			'obj_serializer': ClassificationSerializer
		})
		if not pk:
			address = Address(**data['primary_address'])
			address.save()
			data['personal_data']['primary_address'] = address
			personal_data = PersonalData(**data['personal_data'])
			personal_data.save()

			# create the address
			caregiver = CareGiver(personal_data=personal_data)
			caregiver.save()
			
			serialiser = CareGiverSerializer(caregiver)
			return Response({'result':serialiser.data}, status=status.HTTP_201_CREATED)
		else:
			personal_data = PersonalData.objects.get(id=pk)
			address = personal_data.primary_address
			# apply update on address
			for key, value in data['primary_address'].items():
				setattr(address, key, value)
			address.save()
			data['personal_data']['primary_address'] = address
			
			for key, value in data['personal_data'].items():
				setattr(personal_data, key, value)
			personal_data.save()
			serialiser = PersonalDataSerializer(personal_data)
			return Response({'result': None},status=status.HTTP_201_CREATED)

	elif request.method == 'GET':
		obj = PersonalData.objects.get(id=pk)
		serialiser = PersonalDataSerializer(obj)
		return Response({'result': serialiser.data})
	

class CareGiverList(APIView):
	"""
		List all CareGiver, or create a new Caregiver
	"""
	def get(self, request, format=None):
		care_givers = CareGiver.objects.all()
		serialiser = CareGiverSerializer(care_givers, many=True)
		return Response({'results':serialiser.data})

	def post(self, request, format=None):
		serialiser = CareGiverSerializer(data=request.data)
		print(serialiser)
		if serialiser.is_valid():
			serialiser.save()
			return Response(serialiser.data, status=status.HTTP_201_CREATED)
		return Response(serialiser.errors, status=status.HTTP_400_BAD_REQUEST)

class CareGiverDetail(APIView):
	"""
	Retrieve, update or delete a caregiver instance
	"""
	def get_object(self, pk):
		try:
			return CareGiver.objects.get(id=pk)
		except CareGiver.DoesNotExist:
			raise Http404

	def get(self, request, pk, format=None):
		care_giver = self.get_object(pk)
		serialiser = CareGiverSerializer(care_giver)
		return Response(serialiser.data)

	def put(self, request, pk, format=None):
		care_giver = self.get_object(pk)
		serialiser = CareGiverSerializer(care_giver, data=request.data)
		if serialiser.is_valid():
			serialiser.save()
			return Response(serialiser.data)
		return Response(serialiser.errors, status=status.HTTP_400_BAD_REQUEST)

	def delete(self, request, pk, format=None):
		care_giver = self.get_object(pk)
		care_giver.delete()
		return Response(status=status.HTTP_204_NO_CONTENT)
		

class ClassificationList(APIView):
	"""
		List all Classification, or create a new Caregiver
	"""
	def get(self, request, format=None):
		classification = Classification.objects.all()
		serialiser = ClassificationSerializer(classification, many=True)
		return Response({'results':serialiser.data})

	def post(self, request, format=None):
		serialiser = ClassificationSerializer(data=request.data)
		if serialiser.is_valid():
			serialiser.save()
			return Response(serialiser.data, status=status.HTTP_201_CREATED)
		return Response(serialiser.errors, status=status.HTTP_400_BAD_REQUEST)
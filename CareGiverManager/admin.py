from django.contrib import admin
from CareGiverManager.models import *


class CareGiverAdmin(admin.ModelAdmin):
	list_display = ('firstname', 'middlename', 'lastname', 'email', 'primary_address', 'status')

	def firstname(self, obj):
		return obj.personal_data.firstname

	def middlename(self, obj):
		return obj.personal_data.middlename

	def lastname(self, obj):
		return obj.personal_data.lastname

	def email(self, obj):
		return obj.personal_data.email

	def secondary_phone(self, obj):
		return obj.personal_data.secondary_phone

	def primary_address(self, obj):
		return obj.personal_data.primary_address


class PersonalDataAdmin(admin.ModelAdmin):
	fieldsets = (
		('Personal Information', {
			'fields':('avatar', 'firstname', 'middlename', 'lastname', 'gender', 'birth_date', 'state_licence_number')
		}),
		('Contact Information', {
			'fields': ('email', 'primary_phone', 'secondary_phone', 'primary_address', 'secondary_address')
		}),
		('Administration', {
			'fields':('classification', 'hired_date')
		})
	)
	list_display = ('firstname', 'middlename', 'lastname', 'email', 'primary_phone', 'primary_address')
# Register your models here.
admin.site.register(Classification)
admin.site.register(PersonalData, PersonalDataAdmin)
admin.site.register(CareGiver, CareGiverAdmin)
from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from authentication.views import api_login, api_logout, api_profile

urlpatterns = [
	url(r'^login/$', api_login, name='api login'),
	url(r'^logout/$', api_logout, name='api logout'),
	url(r'^get-profile/$', api_profile, name='api profile'),
]


urlpatterns = format_suffix_patterns(urlpatterns)
from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view
from django.contrib.auth.decorators import login_required
from rest_framework.response import Response
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect

from authentication.serializers import UserProfileSerializer
from authentication.models import UserProfile

@api_view(['POST'])
def api_login(request, format=None):
	'''
	Authenticate user and logged them into system upon valid credentials
	'''
	if request.method == 'POST':
		print(request.POST)
		try:
			username = request.POST.get('username', '')
			password = request.POST.get('password', '')

			if not username or not password:
				return Response('Username and/or Password missing', status=status.HTTP_400_BAD_REQUEST)
			user = authenticate(username=username, password=password)
			if user is None:
				return Response('Invalid credentials', status=status.HTTP_400_BAD_REQUEST)
			else:
				login(request, user)
				return Response(status=status.HTTP_200_OK)
		except:
			return Response('Invalid credentials', status=status.HTTP_403_FORBIDDEN)
	else:
		return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

@api_view(['POST', 'GET'])
def api_logout(request, format=None):
	if request.method in ['POST', 'GET']:
		if request.user.is_authenticated():
			logout(request)
			request.session.flush()
			return redirect('/', request)
	return Response(status=status.HTTP_403_FORBIDDEN)

@login_required
@api_view(['GET', 'POST'])
def api_profile(request, format=None):
	try:
		user_profile = UserProfile.objects.get(user=request.user)
	except:
		return Response('Profile does not exist', status=status.HTTP_400_BAD_REQUEST)
	if request.method == 'GET':
		serializer = UserProfileSerializer(user_profile)
		return Response({'result': serializer.data})

	elif request.method == 'POST':
		serializer = UserProfileSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
		


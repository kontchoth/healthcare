from django.contrib.auth.models import User
import logging
from rest_framework import serializers
from .constants import AVATAR_DIR

from .models import *

class UserSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		exclude = ('id',)

class UserProfileSerializer(serializers.ModelSerializer):
	user = UserSerializer()
	avatar = serializers.SerializerMethodField()

	class Meta:
		model = UserProfile

	def get_avatar(self, user_profile):
		avatar = user_profile.avatar
		if avatar:
			return '/media/avatars' + avatar.url.split('avatars')[-1]
		return ''


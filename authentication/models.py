from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from .constants import *
from django.db import IntegrityError
from uuid import uuid4
from localflavor.us.models import *
from django.utils.translation import ugettext_lazy as _
from healthcare.models import Address

class UserProfile(models.Model):
	id = models.CharField(primary_key=True, max_length=32, editable=False)
	user = models.OneToOneField(User, related_name='+', null=False)
	avatar = models.ImageField(upload_to=AVATAR_DIR, null=True, blank=True)
	phone_number = PhoneNumberField()
	# email = models.EmailField(max_length=100, unique=True, blank=False, null=False)
	primary_address = models.ForeignKey(Address, blank=False, null=False)
	secondary_address = models.ForeignKey(Address, related_name='+', blank=True, null=True)


	def __str__(self):
		'''
		Return user name in the format <first name> <last name> or <username>
		'''
		name = '{} {}'.format(self.user.first_name.strip(), self.user.last_name.strip())
		if len(name.strip()):
			return name
		return self.user.username

	def get_email(self):
		return self.user.email

	def get_phone_number(self):
		return self.phone_number

	def get_avatar(self):
		return self.avatar

	def save(self, *args, **kwargs):
		'''
		Create a UUID for the profile and make sure it is unique
		'''
		if self.id:
			super(UserProfile, self).save(*args, **kwargs)
			return
		unique = False
		while not unique:
			try:
				self.id = uuid4().hex
				super(UserProfile, self).save(*args, **kwargs)
				unique = True
			except IntegrityError:
				self.id = uuid4().hex


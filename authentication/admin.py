from django.contrib import admin
from authentication.models import *


class AddressAdmin(admin.ModelAdmin):
	fields = ('address_line_1', 'city', 'state', 'zipcode')
	
# Register your models here.
admin.site.register(UserProfile)
admin.site.register(Address)
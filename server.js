/**
 * Simple static content file servers.
 *  - Uses Express directly, if in production mode,
 *  - Uses webpack-dev-server if in dev mode. webpack-dev-server is a wrapper
 *       around express which allows for Live Updates.
 */
var webpack = require('webpack');
if ((process.argv.length === 3) && (process.argv[2] === "prod")) {
  console.log("Loading Prod configuration");
  var config = require('./webpack.prod.config');
  var express = require('express');
  var app = express();
  var port = config.server_info.port;
  app.use("/assets/",
          express.static(__dirname + config.server_info.server_root));

  console.log('Listening at', config.server_info);
  app.listen(port);
} else {
  console.log("Loading Debub configuration");
  var WebpackDevServer = require('webpack-dev-server');
  var config = require('./webpack.debug.config');
  var port = config.server_info.port;
  var host = config.server_info.hostname;
  // Boot up the server, and start serving the static content
  new WebpackDevServer(webpack(config), {
    publicPath: config.output.publicPath,
    hot: true,
    inline: true,
    historyApiFallback: true,
    headers: { "Access-Control-Allow-Origin": "*", "Access-Control-Allow-Credentials": "true" }
  }).listen(port, host, function (err, result) {
    if (err) {
      console.log(err);
    }
    console.log('Listening at', config.server_info);
  });
}

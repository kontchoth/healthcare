"""healthcare URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from healthcare import views as mainView
from CareGiverManager import urls as CareGiverUrls
from authentication import urls as authenticationUrls


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', mainView.login),
    url(r'^home$', mainView.home),
    url(r'^dashboard$', mainView.dashboard),
    url(r'^employees$', mainView.employees),
    url(r'^api/', include(CareGiverUrls)),
    url(r'^api/authentication/', include(authenticationUrls)),
]

from __future__ import unicode_literals

import datetime
from django.db import models
from django.contrib.auth.models import User
from .constants import *
from django.db import IntegrityError
from uuid import uuid4
from localflavor.us.models import *
from django.utils.translation import ugettext_lazy as _

class Address(models.Model):
	TYPES_CHOICE = (
		('HOME', _('Home')),
		('WORK', _('Work')),
		('OTHER', _('Other'))
	)
	address_type = models.CharField(_('Type'), max_length=20, choices=TYPES_CHOICE, default='Home')
	address_line_1 = models.CharField(_('Address 1'), max_length=100, blank=False, null=False)
	address_line_2 = models.CharField(_('Address 2'), max_length=100, blank=True, null=True)
	city = models.CharField(_('City'), max_length=100, blank=False, null=False)
	state = models.CharField(_('State'), max_length=100, blank=False, null=False)
	zipcode = models.CharField(_('Zip Code'), max_length=5, blank=False, null=False)
	country = models.CharField(_('Country'), max_length=100, blank=False, null=False)
	
	def __str__(self):
		return '{} {}, {} {}'.format(self.address_line_1, self.city, self.state, self.zipcode)



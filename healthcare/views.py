from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext

def login(request):
	if not request.user.is_authenticated():
		return render_to_response('healthcare.login.html', RequestContext(request))
	else:
		return redirect('/home', request)

def dashboard(request):
	if not request.user.is_authenticated():
		return redirect('/', request)
	else:
		return render_to_response('healthcare.dashboard.html', RequestContext(request))

def home(request):
	if not request.user.is_authenticated():
		return redirect('/', request)
	else:
		return render_to_response('healthcare.home.html', RequestContext(request))

def employees(request):
	if not request.user.is_authenticated():
		return redirect('/', request)
	else:
		return render_to_response('healthcare.employees.html', RequestContext(request))
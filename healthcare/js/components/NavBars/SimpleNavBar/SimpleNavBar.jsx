import CSSModules from 'react-css-modules';
import styles from './SimpleNavBar.css';
var React = require('react');
var $ = require('jquery');
var _ = require('lodash');
var utils = require('../../../common/utils');

var SimpleNavBar = React.createClass({
	propTypes: {
		user: React.PropTypes.object.isRequired,
		menus: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
		activeMenu: React.PropTypes.string.isRequired,
	},
	componentDidMount: function() {
		
	},
	render: function(){
		let {user, menus, activeMenu} = this.props;
		return (
			<div>
				<div styleName='nav-wrapper'>
					<div styleName='left-panel'>
						<img src='http://placehold.it/150x60/2c7717/fff?text=LOGO+HERE' />
					</div>
					<div styleName='right-panel'>
						<span styleName='username'>Welcome {user.user.first_name}</span>
						<ul>
							<li><a href='#'>my account</a></li>
							<li><a href='/api/authentication/logout'>logout</a></li>
						</ul>
					</div>
				</div>
				<div styleName='navigation'>
					<ul>
						{menus.map(function(menu, index){
							return (
								<li key={menu.text} styleName={menu.text === activeMenu ? 'active': ''}>
									<a href={menu.url}>{menu.text}</a>
								</li>
							);
						})}
					</ul>
				</div>
			</div>
		);
	}
});

module.exports = CSSModules(SimpleNavBar, styles);
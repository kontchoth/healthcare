import CSSModules from 'react-css-modules';
import styles from './FlatButton.css';

var React = require('react');
var _ = require('lodash');
var $ = require('jquery');

var FlatButton = React.createClass({
	propTypes: {
		text: React.PropTypes.string.isRequired,
		clickAction: React.PropTypes.func.isRequired,
		type: React.PropTypes.string,
		width: React.PropTypes.string,
		active: React.PropTypes.bool,
	},
	getDefaultProps: function() {
		return {
			type: 'primary',
			width: null,
			active: false,
		}
	},
	render: function(){
		let {type, clickAction, text, width, active} = this.props;
		var btnStyle = width ? {width : width} : {};
		var activeStyle = active ? 'active ': '';
		activeStyle += type;
		return (
			<button styleName={activeStyle} onClick={clickAction} style={btnStyle}>
				{text}
			</button>
		);
	}
});

module.exports = CSSModules(FlatButton, styles, {allowMultiple:true});
import CSSModules from 'react-css-modules';
import styles from './PersonalDataForm.css';

var React = require('react');
var _ = require('lodash');
var $ = require('jquery');
var utils = require('../../../common/utils');
var states = require('../../../common/statesList');
var InputFieldText = require('../../../components/InputFields/InputFieldText');
var SelectField = require('../../../components/InputFields/SelectField');
var FlatButton = require('../../../components/Buttons/FlatButton');

var PersonalDataForm = React.createClass({
	propTypes: {
		data: React.PropTypes.object,
		classifications: React.PropTypes.arrayOf(React.PropTypes.object),
		cancelAction: React.PropTypes.func.isRequired,
		personal_data_id: React.PropTypes.string,
	},
	getDefaultProps: function(){
		return {
			personal_data_id: null,
		};
	},
	getInitialState: function(){
		return {
			data: null
		};
	},
	componentDidMount: function() {
		utils.setupCSRFToken();
		this.populateFormData();
	},
	retrievePersonalData: function(){
		let {employee_id} = this.props;
		console.log(employee_id);
	},
	getFormData: function(){
		return {
			personal_data: {
				firstname: this.refs.firstname.getValue(),
				middlename: this.refs.middlename.getValue(),
				lastname: this.refs.lastname.getValue(),
				ssn: this.refs.ssn.getValue(),
				gender: this.refs.gender.getValue()['value'],
				birth_date: this.refs.bday.getValue(),
				email: this.refs.email.getValue(),
				primary_phone: this.refs.phone1.getValue(),
				secondary_phone: this.refs.phone2.getValue(),
				classification: this.refs.classification.getValue(),
				
			},
			primary_address: {
				address_type: 'Home',
				address_line_1: this.refs.address1.getValue(),
				address_line_2: this.refs.address2.getValue(),
				city: this.refs.city.getValue(),
				state: this.refs.state.getValue().value,
				zipcode: this.refs.zipcode.getValue()
			}
		};
	},
	populateFormData: function(){
		let {personal_data_id} = this.props;
		let {refs} = this.refs;
		var self = this;
		if (personal_data_id){
			var options = {
				data: {},
				url: 'api/care-givers/personal-data/'+personal_data_id,
				dataType: 'json',
				type: 'GET'
			}, completed = false;

			utils.ajaxCall(options)
			.success(function(xhr, err, reason){
				completed = true;
			})
			.error(function(xhr, err, reason){
				console.log('Failed');
				completed = false;
			})
			.complete(function(xhr, status){
				if (completed){
					var data = xhr.responseJSON.result;
					// self.setState({user: xhr.responseJSON.result});	
					self.refs.firstname.setValue(data.firstname);
					self.refs.middlename.setValue(data.middlename);
					self.refs.lastname.setValue(data.lastname);
					self.refs.ssn.setValue(data.ssn);
					self.refs.gender.setValue(data.gender);
					self.refs.bday.setValue(data.birth_date);
					self.refs.email.setValue(data.email);
					self.refs.phone1.setValue(data.primary_phone);
					self.refs.phone2.setValue(data.secondary_phone);
					self.refs.classification.setValue(data.classification);
					self.refs.address1.setValue(data.primary_address.address_line_1);
					self.refs.address2.setValue(data.primary_address.address_line_2);
					self.refs.city.setValue(data.primary_address.city);
					self.refs.state.setValue(data.primary_address.state);
					self.refs.zipcode.setValue(data.primary_address.zipcode);
				}
				
			});
			
		} else {
			console.log('New Entry');
			console.log(this.refs);
			_.forEach(this.refs, function(ref){
				console.log(ref.name);
			});
		}
	},
	save: function(e){
		e.preventDefault();
		let {personal_data_id} = this.props;
		var data = this.getFormData();
		var options = {
			data: JSON.stringify(data),
			url: 'api/care-givers/personal-data',
			dataType: 'json',
			contentType: 'application/json',
			type: 'POST'
		}, completed = false;

		if (personal_data_id){
			options.url = options.url + '/' + personal_data_id;
		}
		utils.ajaxCall(options)
		.success(function(xhr, err, reason){
			completed = true;
		})
		.error(function(xhr, err, reason){
			console.log('Failed', err);
			completed = false;
		})
		.complete(function(xhr, status){		
		});
	},
	render: function(){
		let {classifications} = this.props;
		return (
			<form name='AddEmployee'>
				<div className='panel panel-default'>
					<div className='panel-heading'>Personal Information</div>
					<div className='panel-body'>
						<div className='container-fluid' styleName='no-padding'>
							<div className='row'>
								<div className='col-md-4'>
									<InputFieldText ref='firstname' id='firstname' label='First Name' required={true} />		
								</div>
								<div className='col-md-4'>
									<InputFieldText ref='middlename' id='middlename' label='Middle Name' />		
								</div>
								<div className='col-md-4'>
									<InputFieldText ref='lastname' id='lastname' label='Last Name' required={true} />		
								</div>
								<div className='col-md-4'>
									<InputFieldText ref='bday' id='bday' label='Birth Date' type='date' required={true} />		
								</div>
								<div className='col-md-4'>
									<InputFieldText ref='ssn' id='ssn' label='SSN #' required={true} />		
								</div>
								<div className='col-md-4'>
									<SelectField  ref='gender' label='Gender' options={[{value: 'Male', label: 'Male'},{value: 'Female', label: 'Female'}]}/>		
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className='panel panel-default'>
					<div className='panel-heading'>Contact Information</div>
					<div className='panel-body'>
						<div className='container-fluid' styleName='no-padding'>
							<div className='row'>
								<div className='col-md-4'>
									<InputFieldText ref='email' id='email' label='Email' type='email' required={true} />		
								</div>
								<div className='col-md-4'>
									<InputFieldText ref='phone1' id='phone1' label='Primary Phone Number' required={true} type='phone' />		
								</div>
								<div className='col-md-4'>
									<InputFieldText ref='phone2' id='phone2' label='Secondary Phone Number' type='phone' />		
								</div>
							</div>
							<div className='row'>
								<div className='col-md-12'>
									<InputFieldText ref='address1' id='address1' label='Address Line 1' required={true} />		
								</div>
								<div className='col-md-12'>
									<InputFieldText ref='address2' id='address2' label='Address Line 2'/>		
								</div>
								<div className='col-md-4'>
									<InputFieldText ref='city' id='city' label='City' required={true} />		
								</div>
								<div className='col-md-4'>
									<SelectField  ref='state' id='state' label='State' options={states}/>		
								</div>
								<div className='col-md-4'>
									<InputFieldText ref='zipcode' id='zipcode' label='Zip Code' required={true} />		
								</div>
								<div className='col-md-4'>
									<SelectField  ref='classification' options={classifications} label='Classification'/>		
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className='container-fluid' styleName='no-padding'>
					<div className='col-md-6 text-left'>
						<FlatButton text='Cancel' clickAction={this.props.cancelAction} />
					</div>
					<div className='col-md-6 text-right'>
						<FlatButton text='Save' clickAction={this.save} />
					</div> 
				</div>
			</form>
		);
	}
});

module.exports = CSSModules(PersonalDataForm, styles, {allowMultiple: true});
import CSSModules from 'react-css-modules';
import styles from './AddEmployee.css';
var React = require('react');
var $ = require('jquery');
var _ = require('lodash');
var utils = require('../../../common/utils');
var states = require('../../../common/statesList');
var InputFieldText = require('../../../components/InputFields/InputFieldText');
var SelectField = require('../../../components/InputFields/SelectField');
var FlatButton = require('../../../components/Buttons/FlatButton');
var PersonalDataForm = require('../PersonalDataForm/PersonalDataForm');

var AddEmployee = React.createClass({
	propTypes: {
		personal_data_id: React.PropTypes.string,
	},
	getDefaultProps: function(){
		return {
			personal_data_id: null,
		};
	},
	getInitialState: function() {
		return {
			classification_choices: [],
			actionBtns: [
				'Notes', 'Payroll Infos', 'Reminders', 'Skills', 'History', 
				'Supervised Visits', 'Unavailable', 'Visit History', 
				'Personal Data', 'Attachements', 'Absence', 'Miscellaneous', 
				'Exclusions', 'In-Services'
			],
			activeAction: 'Personal Data',
			employee: this.props.employee
		}	
	},
	componentDidMount: function() {
		utils.setupCSRFToken();
		this.getClassification();	
	},
	getClassification: function(){
		var self = this;
		var options = {
			data: {},
			url: 'api/care-givers/classifications/all',
			dataType: 'json',
			type: 'GET'
		}, completed = false;

		utils.ajaxCall(options)
		.success(function(xhr, err, reason){
			completed = true;
		})
		.error(function(xhr, err, reason){
			console.log('Failed');
			completed = false;
		})
		.complete(function(xhr, status){
			if (completed){
				self.setState({classifications: xhr.responseJSON.results});	
			}
			
		});
	},
	showPanel: function(panel){ 
		this.setState({activeAction: panel});
	},
	getActionButtons: function() { 
		let {actionBtns, activeAction} = this.state;
		var clickAction = this.showPanel;
		var self = this;
		return (
			actionBtns.map(function(action) { 
				return (
					<FlatButton
						type='simple'
						key={action}
						text={action}
						clickAction={clickAction.bind(self, action)}
						width='100px'
						active={action === activeAction}
					/>
				);
			})
		);
	},
	getActiveForm: function() { 
		let {activeAction, employee, classifications} = this.state;
		let {personal_data_id} = this.props;
		if (activeAction === 'Personal Data'){ 
			return (
				<PersonalDataForm
					classifications={classifications}
					cancelAction={this.props.cancelAction}
					personal_data_id={personal_data_id}
				/>
			);
		}
	},
	render: function(){
		let {classifications} = this.state;
		return (
			<div styleName='wrapper'>
				<div styleName='employee-action-panel'>
					{this.getActionButtons()}
				</div>
				{this.getActiveForm()}
			</div>
		);	
	}
});

module.exports = CSSModules(AddEmployee, styles);
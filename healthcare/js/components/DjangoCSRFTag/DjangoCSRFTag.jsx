var React = require('react');
var $ = require('jquery');
var utils = require('../../common/utils');

var DjangoCSRFTag = React.createClass({
	getInitialState() {
    return {
    	csrftoken: utils.getCSRFToken(),
    	style: {display: 'none'}
    };
	},
	render: function(){
		return (
			<div style={this.state.style}>
				<input type="hidden" name="csrfmiddlewaretoken" value={this.state.csrftoken}/>
			</div>
		);
	}
});

module.exports = DjangoCSRFTag;

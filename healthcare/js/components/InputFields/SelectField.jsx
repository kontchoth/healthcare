import CSSModules from 'react-css-modules';
import Select from 'react-select';
import styles from './SelectField.css';


var React = require('react');
var _ = require('lodash');
var $ = require('jquery');

var SelectField = React.createClass({
	propTypes: {
		options: React.PropTypes.arrayOf(React.PropTypes.object),
		label: React.PropTypes.string
	},
	getInitialState: function() {
		return {
			value: ''
		};
	},
	getValue: function(){
		return this.state.value;
	},
	setValue: function(value){
		return this.setState({value: value});
	},
	logChange: function(val){
		this.setState({value: val});
	},
	render: function(){
		let {value} = this.state;
		let {options, label} = this.props;
		return (
			<div styleName='select'>
				<label>{label}</label>
				<Select
				    name="form-field-name"
				    value={value}
				    options={options}
				    onChange={this.logChange}
				/>
			</div>
		);
	}
});

module.exports = CSSModules(SelectField, styles);
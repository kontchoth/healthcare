import CSSModules from 'react-css-modules';
import styles from './CheckBox.css';

var React = require('react');
var _ = require('lodash');
var $ = require('jquery');

var CheckBox = React.createClass({
	propTypes: {
		label: React.PropTypes.string.isRequired
	},
	getValue: function(){
		return this.refs.checkboxTag.checked;
	},
	setValue: function(value){
		this.refs.checkboxTag.checked = value;
	},
	render: function(){
		let {label} = this.props;
		return (
			<label styleName='checkbox-wrapper' onClick={this.getValue}>
				<input type='checkbox' ref='checkboxTag' />
				{label}
			</label>
		);
	}
});

module.exports = CSSModules(CheckBox, styles);
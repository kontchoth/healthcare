import CSSModules from 'react-css-modules';
import styles from './SimpleTable.css';
var React = require('react');
var $ = require('jquery');
var _ = require('lodash');
var utils = require('../../../common/utils');

var SimpleTable = React.createClass({
	proptypes: {
		data: React.PropTypes.arrayOf(React.PropTypes.object),
		headers: React.PropTypes.arrayOf(React.PropTypes.object),
		itemPerPage: React.PropTypes.number,
		location: React.PropTypes.string,
		onSelect: React.PropTypes.func.isRequired,
	},
	getDefaultProps: function() {
		return {
			headers: [],
			location: null,
			itemPerPage: 10,
		};
	},
	getInitialState: function() {
		return {
			pageNumber: 1,
			items: this.props.data,
		};
	},
	getCurrentPageList: function(){
		let {items, pageNumber} = this.state;
		let {itemPerPage, data} = this.props;
		var min = (pageNumber - 1) * itemPerPage,
		max = min + itemPerPage;
		return data.slice(min, max);
	},
	getHeader: function(headers){
		var headerElement = headers.map(function(header, index){
			return (<th key={index}>{header.text}</th>);
		});
		return headerElement;
	},
	clickAction: function(id) {
		this.props.onSelect(id);
	},
	render: function(){
		var self = this;
		let {headers, data, location} = this.props;
		if (data.length === 0) {
			return (<p>No data found</p>);
		} else {
			var items = this.getCurrentPageList();
			var header = this.getHeader(headers);
			
			return (
				<table className='table table-bordered' styleName='table' cellsapcing='0' width='100%'>
					<thead>
						<tr>{header}</tr>
					</thead>
					<tbody>
						{items.map(function(row, index){
							return (
								<tr key={index} onClick={self.clickAction.bind(self, row)}>
									{headers.map(function(header, index1){

										if (location){
											var value = row[location][header.key];
											if (header.subkey){ 
												value = value[header.subkey];
											}
											return (
												<td key={index1}>{value}</td>
											);
										} else { 
											var value = row[header.key];
											if (header.subkey){ 
												value = value[header.subkey];
											}
											return (
												<td key={index1}>{row[header.key]}</td>
											);
										}
									})}
								</tr>
							);
						})}
					</tbody>
					
				</table>
			);
		}
		
	}
});

module.exports = CSSModules(SimpleTable, styles);
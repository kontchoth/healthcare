var ReactDOM = require('react-dom');
var React = require('react');
var App = require('./app');

ReactDOM.render(<App />, document.getElementById('healthcare-app'));
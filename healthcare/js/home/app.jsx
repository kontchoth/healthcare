import CSSModules from 'react-css-modules';
import styles from './app.css';
var React = require('react');
var $ = require('jquery');
var _ = require('lodash');
var utils = require('../common/utils');
var SimpleNavBar = require('../components/NavBars/SimpleNavBar/SimpleNavBar');

var App = React.createClass({
	getInitialState: function() {
		return {
			user: null,
			menus: [
				{'text': 'Home', url: '/home'},
				{'text': 'Dashboard', url: '/dashboard'},
				{'text': 'Clients', url: '/clients'},
				{'text': 'Employees', url: '/employees'},
				{'text': 'schedules', url: '/schedules'},
				{'text': 'Timesheets', url: '/timesheets'},
				{'text': 'Reports', url: '/reports'},
				{'text': 'Help', url: '/help'}
			],
			activeMenu: 'Home',
			apps: [
				{name: 'employees', icon: 'images/icons/nurses.png', url:'/employees'}
			],
		};	
	},
	componentDidMount: function() {
		utils.setupCSRFToken();
		this.getUserDetail();
	},
	getUserDetail: function() {
		var self = this;
		var options = {
			data: {},
			url: 'api/authentication/get-profile',
			dataType: 'json',
			type: 'GET'
		}, completed = false;

		utils.ajaxCall(options)
		.success(function(xhr, err, reason){
			completed = true;
		})
		.error(function(xhr, err, reason){
			console.log('Failed');
			completed = false;
		})
		.complete(function(xhr, status){
			if (completed){
				self.setState({user: xhr.responseJSON.result});	
			}
			
		});
	},
	render: function() {
		let {user, menus, activeMenu, apps} = this.state;
		console.log(activeMenu);
		return (
			<div styleName='wrapper'>
				{
					user ? <SimpleNavBar user={user} menus={menus} activeMenu={activeMenu}/> : null
				}
				<div styleName='content'>
					<span styleName='healthcare-name'>Homehealth name.</span>
					<ul styleName='apps-list'>
						{apps.map(function(app, index){
							return (
								<li key={app.name}>
									<a href={app.url}>
										<div styleName='img-holder'>
											<img src={utils.staticServer() + app.icon} />
										</div>
										<span styleName='app-name'>{app.name}</span>
									</a>
								</li>
							);
						})}
					</ul>
				</div>
			</div>
		);
	}
});

module.exports = CSSModules(App, styles);
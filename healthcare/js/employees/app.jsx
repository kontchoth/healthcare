import CSSModules from 'react-css-modules';
import styles from './app.css';
var React = require('react');
var $ = require('jquery');
var _ = require('lodash');
var utils = require('../common/utils');
var SimpleNavBar = require('../components/NavBars/SimpleNavBar/SimpleNavBar');
var SimpleTable = require('../components/Tables/SimpleTable/SimpleTable');
var FlatButton = require('../components/Buttons/FlatButton');
var AddEmployee = require('../components/Forms/AddEmployee/AddEmployee');

var App = React.createClass({
	getInitialState: function() {
		return {
			user: null,
			menus: [
				{'text': 'Home', url: '/home'},
				{'text': 'Dashboard', url: '/dashboard'},
				{'text': 'Clients', url: '/clients'},
				{'text': 'Employees', url: '/employees'},
				{'text': 'schedules', url: '/schedules'},
				{'text': 'Timesheets', url: '/timesheets'},
				{'text': 'Reports', url: '/reports'},
				{'text': 'Help', url: '/help'}
			],
			activeMenu: 'Employees',
			nurses: [],
			showAddForm: false,
			personal_data_id: null
		};	
	},
	componentDidMount: function() {
		let {user} = this.state;
		utils.setupCSRFToken();
		if (user === null){
			this.getUserDetail();
			this.getListNurse();
		}		
	},
	getUserDetail: function() {
		var self = this;
		var options = {
			data: {},
			url: 'api/authentication/get-profile',
			dataType: 'json',
			type: 'GET'
		}, completed = false;

		utils.ajaxCall(options)
		.success(function(xhr, err, reason){
			completed = true;
		})
		.error(function(xhr, err, reason){
			console.log('Failed');
			completed = false;
		})
		.complete(function(xhr, status){
			if (completed){
				self.setState({user: xhr.responseJSON.result});	
			}
			
		});
	},
	getListNurse: function(){
		var self = this;
		var options = {
			data: {},
			url: 'api/care-givers/all',
			dataType: 'json',
			type: 'GET'
		}, completed = false;

		utils.ajaxCall(options)
		.success(function(xhr, err, reason){
			completed = true;
		})
		.error(function(xhr, err, reason){
			console.log('Failed');
			completed = false;
		})
		.complete(function(xhr, status){
			if (completed){
				self.setState({nurses: xhr.responseJSON.results});	
			}
			
		});
	},
	addNew: function(){
		this.setState({showAddForm: true});
	},
	cancel: function(){
		this.setState({showAddForm: false, employee_id: null});
	},
	selectEmployee: function(employee) {
		console.log('You selected', employee.personal_data.id);
		this.setState({personal_data_id: employee.personal_data.id.toString(), showAddForm: true});
	},
	render: function() {
		let {user, menus, activeMenu, apps, nurses, showAddForm, personal_data_id} = this.state;
		var headers = [
			{text: 'First Name', key: 'firstname'},
			{text: 'Last Name', key: 'lastname'},
			{text: 'Email', key: 'email'},
			{text: 'Classification', key: 'classification', subkey: 'name'},
			{text: 'Phone Number', key: 'primary_phone'},
			{text: 'Date Hired', key: 'hired_date'},
			{text: 'Status', key: 'status'}
		];
		return (
			<div styleName='wrapper'>
				{
					user ? <SimpleNavBar user={user} menus={menus} activeMenu={activeMenu}/> : null
				}
				<div styleName='content'>
					
					{	showAddForm ?
							<div>
								<AddEmployee cancelAction={this.cancel} personal_data_id={personal_data_id}/>
							</div>
						:
							<div>
								<div styleName='action-row'>
									<FlatButton text='Add new' clickAction={this.addNew}/>
								</div>
								<SimpleTable data={nurses} headers={headers} location='personal_data' onSelect={this.selectEmployee}/>
							</div>
					}
				</div>
			</div>
		);
	}
});

module.exports = CSSModules(App, styles);
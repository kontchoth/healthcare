import CSSModules from 'react-css-modules';
import styles from './app.css';
var React = require('react');
var $ = require('jquery');
var DjangoCSRFTag = require("../components/DjangoCSRFTag/DjangoCSRFTag");
var utils = require('../common/utils');
var LinkedStateMixin = require('react-addons-linked-state-mixin');
var InputFieldText = require('../components/InputFields/InputFieldText');
var FlatButton = require('../components/Buttons/FlatButton');
var CheckBox = require('../components/InputFields/CheckBox');

var App = React.createClass({
	mixins:[LinkedStateMixin],
	getInitialState: function(){
		return {
			formInputs: [
				{label:'username',type: 'text',id: 'username'},
				{label:'password',type: 'password',id: 'password'}
			],
			error: ''
		};
	},
	componentDidMount: function(){
		utils.setupCSRFToken();
	},
	login: function(event){
		event.preventDefault();
		var self = this;
		var options = {
			data: {
				username: this.refs.username.getValue(),
				password: this.refs.password.getValue()	
			},
			url: '/api/authentication/login/',
			dataType: 'json',
			type: 'POST'
		};
		
		utils.ajaxCall(options)
		.success(function(xhr, err, reason){
			window.location.href = '/home';
		})
		.error(function(xhr, err, reason){
			self.setState({error: xhr.responseText});
		});

	},
	render: function(){
		return( 
			<div styleName='login-page'>
				<div styleName='left-area'>

				</div>
				<div styleName='right-area' className='login-panel'>
					<span className='login-error'>{this.state.error}</span>
					<form name='LoginForm' method='POST'>
						<DjangoCSRFTag />
						<InputFieldText label='Username' id='username' required={true} ref='username' type='text'/>
						<InputFieldText label='Password' id='password' required={true} ref='password' type='password'/>
						<div className='container-fluid' styleName='no-padding'>
							<div className='col-md-6' styleName='no-padding'>
								<CheckBox label='Remember me' ref='rememberMe' />
							</div>
							<div className='col-md-6 text-right' styleName='no-padding'>
								<FlatButton text='login' type='primary' clickAction={this.login} />
							</div>
						</div>
					</form>
				</div>
			</div>
		);	
	}
	
});

module.exports = CSSModules(App, styles);
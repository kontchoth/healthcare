import CSSModules from 'react-css-modules';
import styles from './app.css';
var React = require('react');
var $ = require('jquery');
var _ = require('lodash');
var utils = require('../common/utils');
var SimpleNavBar = require('../components/NavBars/SimpleNavBar/SimpleNavBar');

var App = React.createClass({
	getInitialState: function() {
		return {
			user: null,
			menus: [
				{'text': 'Home', url: '/home'},
				{'text': 'Dashboard', url: '/dashboard'},
				{'text': 'Clients', url: '/clients'},
				{'text': 'Employees', url: '/employees'},
				{'text': 'schedules', url: '/schedules'},
				{'text': 'Timesheets', url: '/timesheets'},
				{'text': 'Reports', url: '/reports'},
				{'text': 'Help', url: '/help'}
			],
			activeMenu: 'Dashboard'
		};	
	},
	componentDidMount: function() {
		utils.setupCSRFToken();
		this.getUserDetail();
	},
	getUserDetail: function() {
		var self = this;
		var options = {
			data: {},
			url: 'api/authentication/get-profile',
			dataType: 'json',
			type: 'GET'
		}, completed = false;

		utils.ajaxCall(options)
		.success(function(xhr, err, reason){
			completed = true;
		})
		.error(function(xhr, err, reason){
			console.log('Failed');
			completed = false;
		})
		.complete(function(xhr, status){
			if (completed){
				self.setState({user: xhr.responseJSON.result});	
			}
			
		});
	},
	render: function() {
		let {user, menus, activeMenu} = this.state;
		return (
			<div styleName='wrapper'>
				{
					user ? <SimpleNavBar user={user} menus={menus} activeMenu={activeMenu}/> : null
				}
				Welcome to your dashboard
			</div>
		);
	}
});

module.exports = CSSModules(App, styles);
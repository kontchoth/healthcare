'use stricts';

var path = require('path');
var webpack = require('webpack');
var BundleTracker = require('webpack-bundle-tracker');
var ExtractTextPlugin = require('extract-text-webpack-plugin');


module.exports = {
	context: __dirname,
	// entry point of our app. assets/js/index.js should require other js modules and dependencies it needs
	entry: {
		healthcare_login_view: './healthcare/js/login/main',
		healthcare_dashboard_view: './healthcare/js/dashboard/main',
		healthcare_home_view: './healthcare/js/home/main',
		healthcare_employees_view: './healthcare/js/employees/main',
	},

	output: {
		path: path.resolve('./assets/bundles/'),
		filename: '[name]-[hash].js',
		publicPath: 'http://localhost:3001/assets/bundles/'
	},

	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoErrorsPlugin(),
		new BundleTracker({filename: './webpack-stats.json'}),
	],

	module: {
		loaders: [
			{ test: require.resolve("jquery"), loader: "imports?jQuery=jquery" },
			{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				// to transform JSX into JS
				include: path.join(__dirname, 'src'),
				loaders: ['react-hot','babel-loader'],
				query: {
					presets:['es2016', 'react']
				}
			},{
				test: /\.css$/,
				loader: ExtractTextPlugin.extract('css?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]'),
				include: path.join(__dirname, '/src'),
			},{
				test: /.(jpg|woff(2)?|eot|ttf|svg)(\?[a-z0-9=\.]+)?$/,
				loader: 'file-loader'
			}, {
				test: /\.png$/,
				loader: "url-loader?limit=100000"
			},{
				test: /\.json$/, loader: 'json'
			}
		],
	},

	resolve: {
		modulesDirectories: ['node_modules', 'bower_components'],
		extensions: ['', '.js', '.jsx']
	},
	server_info: {
		connection_type: 'http',
		hostname: 'localhost',
		port: '3001',
		server_root: '/assets/'
	}
};
